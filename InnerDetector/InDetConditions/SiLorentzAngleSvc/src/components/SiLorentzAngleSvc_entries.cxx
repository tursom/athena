#include "../SiLorentzAngleTool.h"
#include "../SCTSiLorentzAngleCondAlg.h"
#include "../PixelSiLorentzAngleCondAlg.h"

DECLARE_COMPONENT( SiLorentzAngleTool )
DECLARE_COMPONENT( SCTSiLorentzAngleCondAlg )
DECLARE_COMPONENT( PixelSiLorentzAngleCondAlg )

